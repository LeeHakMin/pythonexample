from bs4 import BeautifulSoup
import urllib.request
import urllib.error
import win32com.client

def getReferenceDict(wb_ref):    
    ws_ref = wb_ref.Worksheets('ref')

    refDict = {}
    endText= 'ENDCOLUMNLIST'
    i = 1
    while(ws_ref.Cells(i,1).Text != endText):
        if(ws_ref.Cells(i,2).Text != ''):
            refDict[ws_ref.Cells(i,1).Text] = ws_ref.Cells(i,2).Text    
        i += 1
    return refDict;

def getPlayerList(wb_ref):
    ws_player = wb_ref.Worksheets('players')
    playerList = list()
    endText= 'ENDCOLUMNLIST'
    i = 1
    while(ws_player.Cells(i,1).Text != endText):
        playerList.append(ws_player.Cells(i,1).Text.strip())
        i = i+1
    return playerList;

def parseUrl(a):
    text = a['href'].split("/")    
    pageText = text[4].split(".")    
    return [baseDomain + "/" + text[3] + "/" + pageText[0] + "." + pageText[1] + "." + str(year) + "." + pageText[2] for year in range(2008,2018)]

def parseUrl_Other(a, _ws_res):
    text = a['href'].split("/")
    cateUrl = baseDomain + "/" + text[3] + "/" + text[4]
    soup = BeautifulSoup(urllib.request.urlopen(cateUrl).read(), "html.parser")
    content = soup.find(class_='section categories')
    aList = content.find_all('a', class_=False)
    global column
    for a in aList:        
        chkFlag = True

        for url in parseUrl(a):            
            titleCell = _ws_res.Cells(1,column).Value
            print(url)                        
            inputYear = url.split('.')[-2]           
            if(titleCell != None and inputYear == '2008'):                
                chkFlag = False
                pass          
            elif(inputYear != '2008' and chkFlag == False):
                pass
            
            else:                
                createFile(url, _ws_res, column)

        column+=1
        
    return;

def writePlayerList(_ws_res):    
    _ws_res.Cells(1,1).Value = 'YEAR'
    _ws_res.Cells(1,2).Value = 'PLAYER NAME'
    i = 2
    for year in range(2008,2018):                
        for player in playerList:
            _ws_res.Cells(i,1).Value = year
            _ws_res.Cells(i,2).Value = player
            i+=1
    return;

def createFile(url, _ws_res, _column):      
    try:
        soup = BeautifulSoup(urllib.request.urlopen(url).read(), "html.parser")
    except urllib.error.HTTPError:
        return;
    
    #년도 찾기
    splitUrl = url.split('.')
    inputYear = splitUrl[-2]      
        
    table = soup.find(id='statsTable')    

    #Title
    try:
        title = soup.find(class_='main-content-off-the-tee-details').find('h3').get_text()
    except AttributeError:
        return;
    
    _ws_res.Cells(1,_column).Value = title
        
    # header   
    theads = table.find('thead').find_all('th')

    headerList = list()
    for th in theads:
        headerList.append(th.get_text().upper())

    targetValue = reference.get(title.upper())    
    if(headerList.count(targetValue) > 0):
        headerIndex = headerList.index(targetValue)
    else:
        return;
    
    playerIndex = headerList.index('PLAYER NAME') 

    endText= 'ENDCOLUMNLIST'

    rows = table.find('tbody').find_all('tr')
            
    i = 2
    while(_ws_res.Cells(i,1).Text != endText):        
        if(_ws_res.Cells(i,1).Text == inputYear):
            player = _ws_res.Cells(i,2).Text
            targetAnchor = table.find('a', string=player)
            if(targetAnchor != None):
                row = targetAnchor.parent.parent
                try:
                    _ws_res.Cells(i, _column).Value = row.find_all('td')[headerIndex].get_text()
                except IndexError:
                    pass
        else:
            pass
                
        i+=1
    return;

# main

excel = win32com.client.Dispatch("Excel.Application")
excel.visible = True
wb = excel.Workbooks.Open('D:\\crawling\\result.xlsx')

wb_ref = excel.Workbooks.Open('D:\\crawling\\reference.xlsx')
reference = getReferenceDict(wb_ref)
playerList = getPlayerList(wb_ref)

ws_res = wb.Worksheets('result')

#writePlayerList(ws_res)

baseDomain = "https://www.pgatour.com"
targetUrl = "https://www.pgatour.com/stats.html"

soup = BeautifulSoup(urllib.request.urlopen(targetUrl).read(), "html.parser")
navArea = soup.find('ul', class_='nav nav-tabs slim nav-tabs-drop')

aList = navArea.find_all('a')

column = 3
for a in aList:
    text = a['href'].split("/")
    #Overview Skip
    if(len(text) == 4):
        pass
    else:
        parseUrl_Other(a, ws_res)

wb.Save()
excel.Quit()

    

